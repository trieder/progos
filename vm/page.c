#include "page.h"
#include "threads/thread.h"
#include "threads/malloc.h"
#include "threads/vaddr.h"
#include "threads/interrupt.h"
#include "userprog/pagedir.h"

/* Returns a hash value for page p. */
unsigned
page_hash (const struct hash_elem *p_, void *aux UNUSED)
{
  const struct page *p = hash_entry (p_, struct page, hash_elem);
  return hash_bytes (&p->addr, sizeof p->addr);
}

/* Returns true if page a precedes page b. */
bool
page_less (const struct hash_elem *a_, const struct hash_elem *b_,
           void *aux UNUSED)
{
  const struct page *a = hash_entry (a_, struct page, hash_elem);
  const struct page *b = hash_entry (b_, struct page, hash_elem);

  return a->addr < b->addr;
}

/* Returns the page containing the given virtual address,
   or a null pointer if no such page exists. */
struct page *
page_lookup (const void *address)
{
  struct thread *t = thread_current();
  struct page p;
  struct hash_elem *e;

  p.addr = pg_round_down(address);
  e = hash_find (&t->page_table, &p.hash_elem);
  return e != NULL ? hash_entry (e, struct page, hash_elem) : NULL;
}

struct page* build_page(uint8_t *addr, int type)
{
  struct page* upage = malloc(sizeof(struct page));

  upage->addr = pg_round_down(addr);
  upage->type = type;

  return upage;
}

void add_page (struct page* page)
{
  struct thread *t = thread_current();
  //printf("inserting page with offset %d\n", page->ofs);
  hash_insert(&t->page_table, &page->hash_elem);
}

bool remove_page (const void *address) {
  struct thread *t = thread_current();
  struct page *p = page_lookup(address);
  if (p == NULL) {
    return false;
  }
  hash_delete(&t->page_table, &p->hash_elem);

  pagedir_clear_page(t->pagedir, address);
  return true;
}
