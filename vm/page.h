#ifndef PAGE_H
#define PAGE_H

#include <hash.h>
#include <debug.h>
#include "filesys/off_t.h"
#include <stdbool.h>


#define DEMAND_PAGE 1
#define ZERO_PAGE 2
#define PARTIAL_PAGE 4

#define USER_BOTTOM_ADDR ((void *) 0x08048000)
#define STACK_END ((void *) LOADER_PHYS_BASE)
#define STACK_BEGINNING ((void *) 0xbf800000)

// 8 MB Stack Limit
#define STACK_LIMIT (8 * 1024 * 1024) 

struct page
  {
    struct hash_elem hash_elem; /* Hash table element. */
    void *addr;                 /* Virtual address. */
    void *kaddr;                /* kernel address */
    struct thread *owner;       /* thread owner */
    uint8_t type;               /* type of the page - see defines above */
    off_t ofs;                  /* offset - taken from process.c */
    bool writable;              /* writable flag - taken from process.c */
    size_t read_bytes;          /* number of read bytes - taken from process.c */
    size_t zero_bytes;          /* number of zero bytes - taken from process.c */
    struct file *file;          /* the file where to read from */
  };


struct page* build_page(uint8_t *addr, int type);
void add_page (struct page* page);

unsigned page_hash (const struct hash_elem *p_, void *aux UNUSED);
bool page_less (const struct hash_elem *a_, const struct hash_elem *b_, void *aux UNUSED);
struct page * page_lookup (const void *address);
bool remove_page (const void *address);

#endif