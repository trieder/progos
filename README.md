# Operating System Development #

Welcome to the Operating System Development course at Vienna. In this course, you will be working with an adapted version of the Pintos operating system, which was written by Ben Pfaff (See section 1.4 Acknowledgements.)
So ... Welcome to Pintos. Pintos is a simple operating system framework for the 80x86 architecture. It supports kernel threads, loading and running user programs, and a file system, but it implements all of these in a very simple way. In the Pintos projects, you and your project team will strengthen its support in some of these areas.


## Assignments ##

 * Project 0: Sleeping for threads and argument passing
 * Project 1: Priority scheduling
 * Project 2: Paging/Virtual memory

## Team ##

 * Jakob Englisch
 * Florian Mayer
 * Thomas Rieder